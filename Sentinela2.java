package meuRobo;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Sentinela2 - a robot by (your name here)
 */
public class Sentinela2 extends AdvancedRobot
{

boolean andandoFrente;
	boolean parede;
	/**
	 * run: Sentinela2's default behavior
	 */
public void run() {
//Escolhendo COres do robô -------------------------------------------------
			setBodyColor(Color.darkGray);   //corpo
			setGunColor(Color.cyan);        //canhão
			setRadarColor(Color.lightGray); //radar
			setScanColor(Color.pink);       //scaner
			setBulletColor(Color.red);      //disparo
			
//Permite movimentação idependente, caso verdadeiro--------------------------
			setAdjustRadarForGunTurn(true);
			setAdjustRadarForRobotTurn(true);
			setAdjustGunForRobotTurn(true);
//----------------------------------------------------------------------------	

// verificar se está mais perto que 50px das BORDAS---------------------------
	   if (getX() <= 100 || getY() <= 100
						 || getBattleFieldWidth() - getX() <= 100
						 || getBattleFieldHeight() - getY() <= 100){
			this.parede = true;			
		}else {
			this.parede = false;
		}
//-----------------------------------------------------------------------------

//andar para frente e verificar se esta andando para frente--------------------
		setAhead(1000000);
		this.andandoFrente = true;
		
//-----------------------------------------------------------------------------

// Robot main loop-------------------------------------------------------------
		while(true) {
				//verificar se ja afastou da parede e continua true indicando
				//que não afastou, se sim, trocar para false indicando
				// que afastou
				if (getX() > 100 && getY() > 100
								&& getBattleFieldWidth()  - getX() > 100
								&& getBattleFieldHeight() - getY() > 100 
								&& this.parede == true){
					this.parede = false;	
				}

				//fazer a verificação, trocar a direção e o bool
				 if (getX() <= 100 || getY() <= 100
								  || getBattleFieldWidth()  - getX() <= 100
								  || getBattleFieldHeight() - getY() <= 100){
					if (this.parede == false){
							reveDirecao();
							parede = true;
					}
				}
				//girar novamente o radar
				setTurnRadarRight(360);

				//executar os comandos set
				execute();
		}
	}
//-----------------------------------------------------------------------------
	public void onScannedRobot(ScannedRobotEvent e) {
			double anguloI = getHeading() + e.getBearing();
			double distanciaI = e.getDistance();
			double direcaoCanhao = anguloI - getGunHeading();
			double direcaoRadar = anguloI - getRadarHeading(); 
			
			setTurnRight(e.getBearing() + 80);		
			setTurnRadarRight(direcaoRadar);
			
			if(distanciaI < 200){
				setTurnGunRight(direcaoCanhao);
				fire(3);
			}else if(distanciaI < 80){
					setBack(100);
			}else if(distanciaI > 200 && distanciaI < 400){				
				setTurnGunRight(direcaoCanhao);
				setTurnRight(e.getBearing() + 30);	
				setAhead(400);
				fire(1);	
			}else{
				setTurnGunRight(direcaoCanhao);
				setTurnRight(e.getBearing() + 30);	
				setAhead(400);
				fire(1);	
				}
	}
//ao colidir com robô, trocar direção------------------------------------------	
	public void onHitRobot(HitRobotEvent e) {
				double direcao = (e.getBearing() + getHeading() - getGunHeading());
				turnGunRight(direcao);
				fire(3);
		
	}
//-----------------------------------------------------------------------------
	public void onHitByBullet(HitByBulletEvent e) {
		
		reveDirecao();
	}
//-----------------------------------------------------------------------------
	public void onHitWall(HitWallEvent e) {
		setBack(500);
		
	}
//-----------------------------------------------------------------------------	
	public void reveDirecao(){
		if (this.andandoFrente){
			setBack(1000000);
			this.andandoFrente = false;
		}else {
			setAhead(1000000);
			this.andandoFrente = true;
			}
	}	
}